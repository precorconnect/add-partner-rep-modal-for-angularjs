## Description
Add partner rep modal for AngularJS.

## Example
refer to the [example app](example) for working example code.

## Setup

**install via jspm**  
```shell
jspm install bitbucket:precorconnect/add-partner-rep-modal-for-angularjs
``` 

**import & wire up**
```js
import 'add-partner-rep-modal-for-angularjs';

angular.module(
            "app",
            ["add-partner-rep-modal.module"]
        )
        // ensure dependencies available in container
        .constant(
            'sessionManager', 
            sessionManager
            /*see https://bitbucket.org/precorconnect/session-manager-for-browsers*/
        )
        .constant(
            'PartnerRepServiceSdk',
            partnerRepServiceSdk
            /*see https://bitbucket.org/precorconnect/partner-rep-service-sdk-for-javascript*/
        );
```