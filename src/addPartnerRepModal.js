import Controller from './controller';
import './default.css!css';
import template from './index.html!text';

export default class AddPartnerRepModal {

    _$modal;

    constructor($modal) {

        if(!$modal){
            throw new TypeError('$modal required');
        }
        this._$modal = $modal;

    }

    /**
     * Shows the add facility contact modal
     * @returns {Promise<number>} partner rep id
     */
    show():Promise<number> {

        let modalInstance = this._$modal.open({
            controller: Controller,
            controllerAs: 'controller',
            template: template,
            backdrop: 'static'
        });

        return modalInstance.result;
    }
}

AddPartnerRepModal.$inject = [
    '$modal'
];