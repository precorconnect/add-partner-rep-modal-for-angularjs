import SessionManager from 'session-manager';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';
import {EmailPartnerRepReq} from 'partner-rep-service-sdk';
import {AdminEmailPartnerRepReq} from 'partner-rep-service-sdk';

export default function repEmailAddressValidator($q,
                                                 partnerRepServiceSdk:PartnerRepServiceSdk,
                                                 sessionManager:SessionManager) {
    return {
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {
            ngModel.$asyncValidators.repEmailAddress = function (modelValue, viewValue) {

                return $q((resolve, reject)=> {
                    sessionManager
                        .getAccessToken()
                        .then(
                            accessToken =>
                                partnerRepServiceSdk
                                    .searchForPartnerRepWithEmailAddress(
                                        viewValue,
                                        accessToken
                                    )
                                    .then(partnerReps => {
                                        if (!partnerReps) {
                                            resolve(true);
                                        }
                                        else {
                                            $q((result) => {
                                                    partnerRepServiceSdk
                                                        .sendEmailToPartnerRep(
                                                            new EmailPartnerRepReq(
                                                                partnerReps.firstName,
                                                                partnerReps.lastName,
                                                                partnerReps.id,
                                                                viewValue,
                                                                partnerReps.sapAccountNumber,
                                                                partnerReps.groupId
                                                            ),
                                                            accessToken)
                                                        .then(sendEmailRes => {
                                                                result(sendEmailRes);
                                                            }
                                                        )
                                                }
                                            )
                                            .then((sendEmailRes) => {
                                                if (sendEmailRes.id == 'EMAIL SENT') {
                                                    resolve(true);
                                                }else
                                                if(sendEmailRes.id == 'EMAIL NOT SENT') {
                                                    $q((result2)=>{
                                                            partnerRepServiceSdk
                                                                .sendEmailToPrecorAdmin(
                                                                    new AdminEmailPartnerRepReq(
                                                                        partnerReps.firstName,
                                                                        partnerReps.lastName,
                                                                        viewValue,
                                                                        partnerReps.sapAccountNumber,
                                                                        partnerReps.groupId
                                                                    ),
                                                                    accessToken)
                                                                .then((response) => {
                                                                        result2(response);
                                                                    }
                                                                ).catch('Error Occurred in Admin Email Required');
                                                        }
                                                    )
                                                    .then((msg)=>{
                                                        scope.backendError = msg.id;
                                                        reject();
                                                        }
                                                    )
                                                }
                                            }
                                            );
                                        }
                                    }
                                )
                        );
                });
            };
        }
    };
}


