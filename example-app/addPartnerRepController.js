import SessionManager from 'session-manager';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';

export default class AddPartnerRepController {

    _sessionManager:SessionManager;

    _addPartnerRepModal;

    _partnerRepServiceSdk:PartnerRepServiceSdk;

    _partnerRep;

    constructor(sessionManager:SessionManager,
                addPartnerRepModal,
                partnerRepServiceSdk:PartnerRepServiceSdk) {

        if (!sessionManager) {
            throw new TypeError('sessionManager required');
        }
        this._sessionManager = sessionManager;

        if (!addPartnerRepModal) {
            throw new TypeError('addPartnerRepModal required');
        }
        this._addPartnerRepModal = addPartnerRepModal;

        if (!partnerRepServiceSdk) {
            throw new TypeError('partnerRepServiceSdk required');
        }
        this._partnerRepServiceSdk = partnerRepServiceSdk;
    }

    get partnerRep() {
        return this._partnerRep;
    }

    showAddPartnerRepModal() {

        this._addPartnerRepModal
            .show()
            .then(partnerRepId => {
                this._partnerRep = {id: partnerRepId};
            })
            .then(() => this._sessionManager.getAccessToken())
            .then(accessToken =>
                this._partnerRepServiceSdk
                    .getPartnerRepWithId(
                        this._partnerRep.id,
                        accessToken
                    )
            )
            .then(partnerRep => {
                    this._partnerRep = partnerRep;
                }
            );

    }

}


AddPartnerRepController.$inject = [
    'sessionManager',
    'addPartnerRepModal',
    'partnerRepServiceSdk'
];
